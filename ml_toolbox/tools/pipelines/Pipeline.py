
from importlib import import_module
from sklearn.pipeline import Pipeline as pip


class Pipeline:
    def __init__(self, preprocessors):
        """
        PreprocessorClass: "StandardScaler"
        PreprocessorName: "MyScaler"
        PreprocessorType: "Standardization"
        copy: "True"
        with_mean: "True"
        with_std: "True"
        
        """
        self.preprocessors = []

        
        for p in preprocessors:
            keys = ['PreprocessorClass', 'PreprocessorName', 'PreprocessorType']

            metadata = {}
            metadata['PreprocessorClass'] = p['PreprocessorClass']
            metadata['PreprocessorName'] = p['PreprocessorName']
            metadata['PreprocessorType'] = p['PreprocessorType']
            args = {k:v for k,v in p.items() if k not in keys}
            metadata['args'] = args

            # '/tools/preprocessing/' +p['PreprocessorType'] + '/'
            import sys
            sys.path.append(".")
            mod = import_module('tools.preprocessing.' +p['PreprocessorType']  )#package='tools'
            met = getattr(mod, p['PreprocessorClass'])
            
            self.preprocessors.append((metadata['PreprocessorName'],met(**metadata['args'])))
        self.my_pipe = pip(self.preprocessors)



    def fit_transform(X, y = None):
        return self.my_pipe.fit_transform(X)


