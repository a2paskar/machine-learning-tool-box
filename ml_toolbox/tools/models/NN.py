import keras
from keras.wrappers.scikit_learn import KerasClassifier
from keras.callbacks import EarlyStopping
from keras.utils.generic_utils import has_arg
from keras.models import Sequential
from keras.layers import BatchNormalization
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D, AveragePooling2D
import pandas as pd

class Keras_Wrapper:

	"""
		Class for building Keras models and Sklearn wrapped Keras models.

		-----------------------------------------------------------
		Attributes:
			self.params:		  Dictionary that contains build, compile and fit parameters for keras model.

			self.layers:		  Dictionary of all Keras compatible layers.

			self.skmodel:  Sklearn wrapped Keras model

		-----------------------------------------------------------
		Functions:
			fit:		Trains Sklearn wrapper Keras model.

			predict_proba:		Makes predictions (probability) using trained Sklearn wrapper Keras model.

			predict:		Makes predictions (class) using trained Sklearn wrapper Keras model.

			_model_build:		returns Keras model (without wrapping it as Sklearn estimator).
		"""

	def __init__(self, params= {}):

		self.params = params
		self.skmodel=None

		#more Keras layers can be added to this dictionary.  Visit Keras documentation for more info
		self.layers = {'dense': Dense, 'batch_norm': BatchNormalization, 'conv2d': Conv2D, 'flatten': Flatten,
				  'dropout': Dropout, 'maxpool': MaxPooling2D, 'avgpool'
				  : AveragePooling2D}

		#Sklearn Keras Wrapper
		self.skmodel = KerasClassifier(build_fn=self._model_build)
		self.is_fit= False


	def fit(self, x,y):
		#Keras does not accept pd.DataFrame as input, only arrays
		if isinstance(x,pd.DataFrame) and isinstance(y, pd.Series):
			x = x.values
			y = y.values
		self.skmodel.fit(x,y,**self.params['fit'])

		self.is_fit= True
		return self

	def predict_proba(self, x, **kwargs):
		assert self.is_fit
		if isinstance(x,pd.DataFrame):
			x = x.values
		return self.skmodel.predict_proba(x, **kwargs)


	def predict(self,x):
		assert self.is_fit
		if isinstance(x,pd.DataFrame):
			x = x.values
		return self.skmodel.predict(x)


#automation of build function required for KerasClassifier wrapper
#in order to grid search hyperparameters using Sklearn RandomizedSearchCV, tunable hyperparameters must be listed as arguments to this build function as shown
#any new hyper_parameters to be grid_searched must be added as parameters to this function
	def _model_build(self, optimizer=None, activation=None, epochs=None, batch_size=None, dropout_rate=None, momentum=None, learn_rate=None, metrics=None, loss=None):

		from keras.models import Sequential
		model = Sequential()
		for layer in self.params['build']:
			for k, v in layer.items():
				try:
					model.add(self.layers[k](**v))
				except Exception as e:
					print('Error: ', e)

		try:
			model.compile(**self.params['compile'])
		except Exception as e:
			print('Error: ', e)
		return model