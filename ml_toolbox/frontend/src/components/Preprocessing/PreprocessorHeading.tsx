import React from 'react';

function PreprocessorHeading(props: {
	name: String;
	fn: Object | any;
	chosen: [Object, Function];
}) {
	const fn = Object.keys(props.fn);
	const [value, setValue] = props.chosen;

	return (
		<th>
			<label htmlFor='types'>{props.name} </label>
			<select
				name='types'
				id='cars'
				onChange={e => {
					setValue({ type: props.name, name: e.target.value });
				}}
				value='Choose a Preprocessor'
			>
				<option value=''>Choose a Preprocessor</option>
				{fn.map(val => {
					return <option value={val}>{val}</option>;
				})}
			</select>
		</th>
	);
}

export default PreprocessorHeading;
