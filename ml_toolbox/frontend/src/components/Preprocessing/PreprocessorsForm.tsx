import React, { useState, useEffect } from 'react';
import './PreprocessorsForm.css';
import PreprocessorHeading from './PreprocessorHeading';
import ArgumentsForm from './ArgumentsForm';
import axios from 'axios';

interface keyable {
	[key: string]: any;
}
const PreprocessorsForm = (props: {
	numPreprocessor: number;
	sklearnPreprocessors: Object | any;
	setNumPreprocessor: Function;
}) => {
	const preprocessor_types = Object.keys(props.sklearnPreprocessors);
	const numPreprocessor = props.numPreprocessor;
	const setNumPreprocessor = props.setNumPreprocessor;
	const [chosenPreprocessor, setPreprocessor] = useState({
		type: '',
		name: '',
	});
	var piplineobjs = [];
	for (var i = 0; i < numPreprocessor; i++) {
		piplineobjs.push({});
	}
	const [pipelineObjects, setPipelineObjects] =
		useState<keyable[]>(piplineobjs);

	const [indexToRemove, setIndexToRemove] = useState(-1);
	useEffect(() => {
		if (indexToRemove !== -1) {
			setPipelineObjects(
				pipelineObjects.filter((obj, i) => i !== indexToRemove)
			);
			setIndexToRemove(-1);
			// setNumPreprocessor(numPreprocessor - 1);
		}
	}, [indexToRemove]);
	useEffect(() => {
		var piplineobjs = [];
		for (var i = 0; i < numPreprocessor; i++) {
			piplineobjs.push({});
		}
		setPipelineObjects(piplineobjs);
	}, [numPreprocessor]);
	return (
		<table className='form'>
			<div className='preprocessors'>
				<tr>
					{/* Get preprocessor types from db and render the th with a with processor types */}
					{preprocessor_types.map(name => {
						return (
							<PreprocessorHeading
								chosen={[chosenPreprocessor, setPreprocessor]}
								name={name}
								fn={props.sklearnPreprocessors[name]}
							></PreprocessorHeading>
						);
					})}
				</tr>
			</div>
			<div className='preprocessor_args'>
				{chosenPreprocessor['type'] !== '' &&
				chosenPreprocessor['name'] !== '' ? (
					<ArgumentsForm
						pipelineObjects={pipelineObjects}
						setPipelineObjects={setPipelineObjects}
						chosenProcessor={chosenPreprocessor}
						arguments={
							props.sklearnPreprocessors[chosenPreprocessor['type']][
								chosenPreprocessor['name']
							]
						}
					></ArgumentsForm>
				) : (
					<div></div>
				)}
			</div>
			<table className='array'>
				<tr>
					{pipelineObjects.map((item, i) => {
						if (pipelineObjects.length > 0) {
							return (
								<th key={i}>
									<button
										onClick={e => {
											e.preventDefault();
											setIndexToRemove(i);
										}}
									>
										{pipelineObjects[i]['PreprocessorName']}
									</button>
								</th>
							);
						} else {
							return (
								<th key={i}>
									<button
										onClick={e => {
											e.preventDefault();

											setIndexToRemove(i);
										}}
									></button>
								</th>
							);
						}
					})}
				</tr>
			</table>
			<button
				onClick={() => {
					const createPreprocessor = async () => {
						const url = 'http://127.0.0.1:8000/api/createPreprocessor/1';
						const data = {
							preprocessors: pipelineObjects,
						};
						console.log('data to send: ', data);
						let res = await axios.post(url, data);
					};
					createPreprocessor();
				}}
			>
				Submit
			</button>
		</table>
	);
};

export default PreprocessorsForm;
