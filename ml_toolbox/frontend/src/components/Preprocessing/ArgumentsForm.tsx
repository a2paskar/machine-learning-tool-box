import React, { useState, useEffect } from 'react';

interface keyable {
	[key: string]: any;
}

const ArgumentsForm = (props: {
	chosenProcessor: { name: string; type: string };
	arguments: any;
	setPipelineObjects: React.Dispatch<React.SetStateAction<keyable[]>>;
	pipelineObjects: keyable[];
}) => {
	const name = props.chosenProcessor['name'];
	const type = props.chosenProcessor['type'];

	const args = props.arguments;
	const setPipelineObjects = props.setPipelineObjects;
	const pipelineObjects = props.pipelineObjects;

	const arg_keys = Object.keys(args);
	console.log('arg keys', arg_keys);

	const initializeForm = () => {
		var initialFormValues: keyable = {};
		initialFormValues['PreprocessorClass'] = name;
		initialFormValues['PreprocessorType'] = type;
		return initialFormValues;
	};

	const [preprocessorFormValues, setPreprocessorFormValues] = useState(() =>
		initializeForm()
	);

	useEffect(() => {
		setPreprocessorFormValues(() => {
			initializeForm();
		});
	}, [name, type]);
	const [currentIndex, setCurrentIndex] = useState(0);
	console.log('form vals: ', preprocessorFormValues);
	console.log('pipeline obj: ', pipelineObjects);
	useEffect(() => {
		for (var i = 0; i < pipelineObjects.length; i++) {
			if (Object.keys(pipelineObjects[i]).length === 0) {
				setCurrentIndex(i);
				break;
			}
		}
	}, [pipelineObjects]);

	return (
		<div className='argsForm'>
			<h3>{name}</h3>
			<div style={{ display: 'block', padding: '5px' }}>
				<label htmlFor='PreprocessorName'>Preprocessor Name: </label>
				<input
					onChange={e => {
						// Creating a dummy object using spread operator
						const name1 = e.target.name;
						const val = e.target.value;
						setPreprocessorFormValues(prevState => ({
							...prevState,
							PreprocessorClass: name,
							PreprocessorType: type,

							[name1]: val,
						}));

						console.log('new form vals: ', preprocessorFormValues);
					}}
					required
					name='PreprocessorName'
					placeholder='A custom name'
				></input>
			</div>

			{Object.keys(args).map((key, i) => {
				return (
					<div style={{ display: 'block', padding: '5px' }} key={i}>
						<label htmlFor={key}>{key} </label>
						<input
							required
							name={key}
							placeholder={args[key]}
							onChange={e => {
								// Creating a dummy object using spread operator
								const name1 = e.target.name;
								const val = e.target.value;
								setPreprocessorFormValues(prevState => ({
									...prevState,

									PreprocessorClass: name,
									PreprocessorType: type,
									[name1]: val,
								}));
							}}
						></input>
					</div>
				);
			})}
			<div style={{ padding: '5px', display: 'block' }}>
				<button
					onClick={e => {
						// Creating a dummy object using spread operator
						e.preventDefault();

						setPipelineObjects(
							pipelineObjects.map((obj, i) => {
								if (i === currentIndex) {
									return preprocessorFormValues;
								} else {
									return obj;
								}
							})
						);
					}}
				>
					Add to Pipeline
				</button>
			</div>
		</div>
	);
};

export default ArgumentsForm;
