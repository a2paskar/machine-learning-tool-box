import React, { useState } from 'react';
interface keyable {
	[key: string]: any;
}
const LayerForm = (props: keyable) => {
	const data = props.formData.data;
	const setShowForm = props.formData.setShowForm;
	const name = props.formData.name;
	const setLayers = props.formData.setLayers;

	const [formData, setFormData] = useState(data);
	console.log('formData', formData);

	return (
		<div>
			<h1>{name}</h1>
			{Object.keys(data).map(key => {
				if (key === 'units') {
					return (
						<div>
							<label htmlFor={key}> {key}</label>
							<input
								onChange={e => {
									const name = e.target.name;
									const val = e.target.value;

									setFormData((prevState: any) => ({
										...prevState,
										[name]: parseInt(val),
									}));
								}}
								name={key}
								type='number'
							></input>
						</div>
					);
				} else {
					return (
						<div>
							<label htmlFor={key}>{key} </label>

							<select
								onChange={e => {
									const name = e.target.name;
									const val = e.target.value;

									setFormData((prevState: any) => ({
										...prevState,
										[name]: val,
									}));
								}}
								name={key}
							>
								<option value='None'> Choose</option>
								{data[key].map((item: string) => {
									return <option value={item}>{item}</option>;
								})}
							</select>
						</div>
					);
				}
			})}

			<button
				onClick={() => {
					setLayers((prevState: any) => ({
						...prevState,
						[name]: formData,
					}));
					setShowForm([false, '']);
				}}
			>
				Save Layer Configuration
			</button>
		</div>
	);
};

export default LayerForm;
