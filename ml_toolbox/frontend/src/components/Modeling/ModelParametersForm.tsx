import React, { useState } from 'react';
interface keyable {
	[key: string]: any;
}
const ModelParametersForm = (props: {
	setNumHiddenLayers: Function;
	kerasParams: keyable;
	setModelParams: Function;
}) => {
	const setNumHiddenLayers = props.setNumHiddenLayers;
	const kerasParams = props.kerasParams;
	const tableHeadings = ['Compilation', 'Fit'];

	const setModelParams = props.setModelParams;

	const handleChange = (e: any) => {
		const name = e.target.name;
		const value = e.target.value;
		setModelParams((prevState: any) => ({
			...prevState,
			[name]: value,
		}));
	};

	return (
		<div>
			<div className='modelForm'>
				<form>
					<div className='input'>
						<label htmlFor='numHiddenLayers'>Number of Hidden Layers: </label>
						<input
							type='number'
							onChange={e => {
								if (isNaN(e.target.valueAsNumber)) {
									setNumHiddenLayers(1);
								} else {
									if (e.target.valueAsNumber >= 0) {
										setNumHiddenLayers(e.target.valueAsNumber);
									}
								}
							}}
							name='numHiddenLayers'
						></input>
					</div>
					<table>
						<tr>
							{tableHeadings.map((element, i) => {
								return (
									<th key={i}>
										<h3 key={i}>{element}</h3>
									</th>
								);
							})}
						</tr>

						<tr>
							{tableHeadings.map((e, j) => {
								if (e === 'Compilation') {
									const params = Object.keys(kerasParams[e]);
									return (
										<td>
											{params.map(p => {
												const args = kerasParams[e][p];
												return (
													<div>
														<label htmlFor={p}> {p}: </label>

														<select onChange={handleChange} name={p}>
															<option value=''>Choose </option>
															{args.map((a: any) => {
																return <option value={a}>{a}</option>;
															})}
														</select>
													</div>
												);
											})}
										</td>
									);
								} else {
									const params = Object.keys(kerasParams[e]);
									return (
										<td>
											{params.map(p => {
												return (
													<div>
														<label htmlFor={p}> {p}: </label>
														<input
															onChange={handleChange}
															type='text'
															name={p}
														></input>
													</div>
												);
											})}
										</td>
									);
								}
							})}
						</tr>
					</table>
				</form>
			</div>
		</div>
	);
};

export default ModelParametersForm;
