import React from 'react';
import './Navbar.css';
import LogoutButton from './LogoutButton';

const Navbar = () => {
	return (
		<div className='nav'>
			<a href='' className='nav-tab'>
				Dataset
			</a>
			<a href='/preprocessing' className='nav-tab'>
				Preprocessing
			</a>
			<a href='/modeling' className='nav-tab'>
				Modeling
			</a>
			<a href='/evaluation' className='nav-tab'>
				Evaluation
			</a>
			<LogoutButton className='nav-tab'></LogoutButton>
		</div>
	);
};

export default Navbar;
