import React from 'react';
import './LogoutButton.css';

//for type script, your props have to have a defined type
const LogoutButton = (props: { className: string }) => {
	return (
		<div>
			<button id='logout' className={props.className}>
				Logout
			</button>
		</div>
	);
};

export default LogoutButton;
