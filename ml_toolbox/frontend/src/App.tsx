import React from 'react';
import './App.css';
import { Route, BrowserRouter } from 'react-router-dom';
import Preprocessing from './views/Preprocessing/Preprocessing';
import Modeling from './views/Modeling/Modeling';
import Evaluation from './views/Evaluation/Evaluation';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import Dashboard from './views/Dashboard/Dashboard';
import ProjectConfiguration from './views/ProjectConfiguration/ProjectConfiguration';
// import Dataset from './views/Dataset/Dataset';

//simply returns react router object  (url-> component mapping) with all Pages
function App() {
	return (
		<BrowserRouter>
			<Route path='/login' exact component={Login}></Route>
			<Route path='/register' exact component={Register}></Route>
			<Route path='/dashboard' exact component={Dashboard}></Route>
			<Route path='/config' exact component={ProjectConfiguration}></Route>
			{/* <Route path='/dataset' exact component={Dataset}></Route> */}
			<Route path='/preprocessing' exact component={Preprocessing}></Route>
			<Route path='/modeling' exact component={Modeling}></Route>
			<Route path='/evaluation' exact component={Evaluation}></Route>
		</BrowserRouter>
	);
}

export default App;
