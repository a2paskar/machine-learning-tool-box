import React from 'react';

const Register = () => {
	return (
		<div>
			<form>
				<h1>Sign Up</h1>
				<p>It's easy</p>
				<div id='name-cred'>
					<input type='text' placeholder='First Name' />
					<input type='text' placeholder='Last Name' />
				</div>
				<div id='email-cred'>
					<input type='email' placeholder='Email' />
				</div>
				<input type='password' placeholder='Enter password' />
				<div>
					<input type='password' placeholder='Confirm password' />
				</div>
				<button>Sign Up</button>
			</form>
		</div>
	);
};

export default Register;
