import React from 'react';

const Dashboard = () => {
	let dateCreated = '25/12/2021';
	let dateUpdated = '1/1/2021';
	let modelName = 'krrkrr';
	let modelVersion = 1;
	return (
		<div>
			<div>
				<h1>Models</h1>
				<table id='model-info'>
					<tr>
						<th>Date Created </th>
						<th>Date Updated </th>
						<th>Model Name </th>
						<th>Model Version </th>
						<th></th>
					</tr>
					<tr>
						<td>{dateCreated}</td>
						<td>{dateUpdated}</td>
						<td>{modelName}</td>
						<td>{modelVersion}</td>
						<td>
							<button>Launch</button>
							<button>Delete</button>
						</td>
					</tr>
				</table>
				<button>Create New Project</button>
			</div>
		</div>
	);
};

export default Dashboard;
