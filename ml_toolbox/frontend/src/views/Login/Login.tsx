import React from 'react';

const Login = () => {
	return (
		<div>
			<div>
				<h2>Machine Learning Toolbox</h2>
				<p>Build, Train and Test Neural Networks with the click of a button</p>
			</div>
			<form id='login'>
				<div id='email'>
					<label htmlFor='mail'>Enter Email: </label>
					<input type='email' id='mail' />
				</div>
				<div>
					<label htmlFor='password'>Enter Password: </label>
					<input type='password' id='password' />
				</div>

				<button id='login'>Log in</button>
			</form>
		</div>
	);
};

export default Login;
