import React, { useState } from 'react';
import axios from 'axios';

const ProjectConfiguration = () => {
	const [formValues, setFormValues] = useState({});

	console.log(formValues);
	return (
		<div>
			<form id='configform' action='/inputdataset'>
				<table>
					<tr>
						<td>
							<label htmlFor='modeltype'>Model Type: </label>
						</td>
						<td>
							<select
								name='modeltype'
								onChange={e => {
									setFormValues(prevState => ({
										...prevState,
										[e.target.name]: e.target.value,
									}));
								}}
								id='modeltype'
							>
								<option value='BinaryClassification'>
									Binary Classification
								</option>
								<option value='Classification'>Classification</option>
							</select>
						</td>

						<td>
							{' '}
							<label htmlFor='modelversion'>Model Version: </label>
						</td>

						<td>
							{' '}
							<input
								id='modelversion'
								name='modelVersion'
								onChange={e => {
									setFormValues(prevState => ({
										...prevState,
										[e.target.name]: e.target.value,
									}));
								}}
								type='number'
							/>
						</td>
					</tr>
					<tr>
						<td>
							{' '}
							<label htmlFor='modelname'>Model Name: </label>
						</td>
						<td>
							{' '}
							<input
								id='modelname'
								name='modelName'
								onChange={e => {
									setFormValues(prevState => ({
										...prevState,
										[e.target.name]: e.target.value,
									}));
								}}
								type='text'
							/>
						</td>
						<td>
							{' '}
							<label htmlFor='modeldescription'>Model Description: </label>
						</td>
						<td>
							{' '}
							<input
								id='modeldescription'
								name='modelDescription'
								onChange={e => {
									setFormValues(prevState => ({
										...prevState,
										[e.target.name]: e.target.value,
									}));
								}}
								type='text'
							/>
						</td>
					</tr>
					<div></div>
					<button
						onClick={async e => {
							var url = 'http://127.0.0.1:8000/api/hello';
							var res = await axios.post(url, formValues);
						}}
						id='midbtn'
					>
						Input Dataset
					</button>
				</table>
			</form>
		</div>
	);
};

export default ProjectConfiguration;
