import React, { useEffect, useState, useCallback } from 'react';
import NavBar from '../../components/shared/Navbar';
import ModelParametersForm from '../../components/Modeling/ModelParametersForm';
import LayerForm from '../../components/Modeling/LayerForm';
import './Modeling.css';
import axios from 'axios';

interface keyable {
	[key: string]: any;
}

const Modeling = () => {
	const [kerasParams, setKerasParams] = useState<keyable>({});
	const [showLayerForm, setShowLayerForm] = useState<any>([false, '']);

	useEffect(() => {
		const fetchKerasParams = async () => {
			const url = 'http://localhost:8000/api/keras/all';
			let res = await axios.get(url);
			console.log('keras params: ', res);
			setKerasParams(res.data.keras);
		};
		fetchKerasParams();
		draw();
	}, []);
	const [numHiddenLayers, setNumHiddenLayers] = useState(1);
	const updatehLayers = useCallback(
		(params: keyable) => {
			if (
				params &&
				Object.keys(params).length === 0 &&
				params.constructor === Object
			) {
				return {};
			} else {
				var layers: keyable = {};
				var h_layers = Array.from(Array(numHiddenLayers).keys());
				const pars = {
					units: 3,
					activation: params['Layers']['activation'],
					kernel_initializer: params['Layers']['Layer weight initializers'],
					bias_initializer: params['Layers']['Layer weight initializers'],
					kernel_regularizer: params['Layers']['Layer weight regularizers'],
					bias_regularizer: params['Layers']['Layer weight regularizers'],
					activity_regularizer: params['Layers']['Layer weight regularizers'],
				};
				layers['Input Layer'] = {
					units: 4,
					activation: params['Layers']['activation'],
					kernel_initializer: params['Layers']['Layer weight initializers'],
					bias_initializer: params['Layers']['Layer weight initializers'],
					kernel_regularizer: params['Layers']['Layer weight regularizers'],
					bias_regularizer: params['Layers']['Layer weight regularizers'],
					activity_regularizer: params['Layers']['Layer weight regularizers'],
				};
				for (let i = 0; i < h_layers.length; i++) {
					layers['Hidden Layer ' + (i + 1)] = pars;
				}
				layers['Output Layer'] = {
					units: 1,
					activation: params['Layers']['activation'],
					kernel_initializer: params['Layers']['Layer weight initializers'],
					bias_initializer: params['Layers']['Layer weight initializers'],
					kernel_regularizer: params['Layers']['Layer weight regularizers'],
					bias_regularizer: params['Layers']['Layer weight regularizers'],
					activity_regularizer: params['Layers']['Layer weight regularizers'],
				};
				return layers;
			}
		},
		[numHiddenLayers]
	);

	const [hLayers, sethLayers] = useState<keyable>(() =>
		updatehLayers(kerasParams)
	);
	console.log('layers array: ', hLayers);

	// setNumHiddenLayers , kerasParams
	function draw() {
		var canvases = document.getElementsByClassName('circle');
		for (let i = 0; i < canvases.length; i++) {
			let canvas = canvases[i] as HTMLCanvasElement;

			if (canvas.getContext) {
				var ctx = canvas.getContext('2d');
				var X = canvas.width / 2;
				var Y = canvas.height / 2;
				var R = 45;
				if (ctx) {
					ctx.beginPath();
					ctx.arc(X, Y, R, 0, 2 * Math.PI, false);
					ctx.lineWidth = 3;
					ctx.strokeStyle = 'blue';

					ctx.stroke();
				}
			}
		}
	}
	const [userHlayers, setUserHlayers] = useState(hLayers);
	useEffect(() => {
		console.log('EFFECTY');
		sethLayers(() => updatehLayers(kerasParams));
		setUserHlayers(() => updatehLayers(kerasParams));
		draw();
	}, [numHiddenLayers, kerasParams, updatehLayers]);

	console.log('user layers array: ', userHlayers);
	useEffect(() => {
		draw();
	}, [userHlayers]);

	const [modelParams, setModelParams] = useState<keyable>({});
	console.log('User model paramesss', modelParams);

	return (
		<div>
			<NavBar></NavBar>
			{kerasParams &&
			Object.keys(kerasParams).length !== 0 &&
			kerasParams.constructor === Object ? (
				<div>
					<ModelParametersForm
						setNumHiddenLayers={setNumHiddenLayers}
						kerasParams={kerasParams}
						setModelParams={setModelParams}
					></ModelParametersForm>
					{showLayerForm[0] ? (
						<LayerForm
							formData={{
								name: showLayerForm[1],
								data: hLayers[showLayerForm[1]],
								setShowForm: setShowLayerForm,
								setLayers: setUserHlayers,
							}}
						></LayerForm>
					) : null}
					<table>
						<tr>
							{Object.keys(userHlayers).map(e => {
								return (
									<th>
										<button
											onClick={ev => {
												setShowLayerForm([true, e]);
											}}
										>
											{e}
										</button>
									</th>
								);
							})}
						</tr>

						<tr>
							{Object.keys(userHlayers).map(e => {
								const arr = Array.from(Array(userHlayers[e]['units']).keys());

								return (
									<td>
										{arr.map(el => {
											return (
												<div>
													<canvas
														className='circle'
														width='100%'
														height='100%'
													></canvas>
												</div>
											);
										})}
									</td>
								);
							})}
						</tr>
					</table>
					<button
						onClick={() => {
							const trainModel = async () => {
								const url = 'http://127.0.0.1:8000/api/train/1';
								const data = {
									compile: modelParams,
									nn: userHlayers,
								};
								console.log('data to send: ', data);
								let res = await axios.post(url, data);
							};
							trainModel();
						}}
					>
						Train Model
					</button>
				</div>
			) : (
				<div></div>
			)}
		</div>
	);
};

export default Modeling;
