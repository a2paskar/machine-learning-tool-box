import './Preprocessing.css';
import Navbar from '../../components/shared/Navbar';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

import PreprocessorsForm from '../../components/Preprocessing/PreprocessorsForm';
const Preprocessing = () => {
	const [numPreprocessor, setNumPreprocessor] = useState(1);
	console.log('numPreprocessor: ', numPreprocessor);

	const [sklearnPreprocessors, setSklearnPreprocessors] = useState({});
	// console.log('PREPROCESSORS: ', sklearnPreprocessors);

	//GET SKLEARN PREPROCESSOR DATA FROM DB
	useEffect(() => {
		const fetchPreprocessors = async () => {
			const url = 'http://127.0.0.1:8000/api/preprocessors/all';
			let res = await axios.get(url);
			setSklearnPreprocessors(res.data['preprocessors']);
		};
		fetchPreprocessors();
	}, []);

	return (
		<div>
			<Navbar></Navbar>
			<div className='content'>
				{/* every page is technically a form... */}
				<form>
					<div className='input'>
						<label htmlFor='num_preprocessors'>Number of Preprocessors: </label>
						<input
							type='number'
							onChange={e => {
								if (isNaN(e.target.valueAsNumber)) {
									setNumPreprocessor(1);
								} else {
									if (e.target.valueAsNumber >= 0) {
										setNumPreprocessor(e.target.valueAsNumber);
									}
								}
							}}
							name='num_preprocessors'
						></input>
					</div>

					<PreprocessorsForm
						sklearnPreprocessors={sklearnPreprocessors}
						numPreprocessor={numPreprocessor}
						setNumPreprocessor={setNumPreprocessor}
					></PreprocessorsForm>
				</form>
			</div>
		</div>
	);
};

export default Preprocessing;
