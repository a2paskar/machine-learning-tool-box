
# Register your models here.
from django.contrib import admin
from .models import SklearnPreprocessors
from .models import Keras
admin.site.register(SklearnPreprocessors)
admin.site.register(Keras)