from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse

from .models import SklearnPreprocessors #db
from .models import Keras #db

from tools import Pipeline
from tools import Keras_Wrapper

import pandas as pd
import json

import pickle #credits to stack overflow user= blender



# Create your views here.
@api_view(['GET'])
def allPreprocessors(request):
    preprocessors = list(SklearnPreprocessors.objects.all().values())
    df = pd.DataFrame(preprocessors)
    preprocessors_types= list(set(df['preprocessor_type']))
    data = {}
    for p in preprocessors_types:
        temp = df.loc[df['preprocessor_type'] == p]
        data[p] = {n:{k[0]:k[1] for k in temp.loc[temp['preprocessor_name'] == n][['arg_name', 'arg_type']].values} for n in list(set(temp['preprocessor_name']))}

    return JsonResponse({"preprocessors": data})


@api_view(['POST'])
def createPipeline(request, id): #modelName
    data = json.loads(request.body.decode('utf-8'))
    my_pipe = Pipeline(data['preprocessors'])
    import sys
    sys.path.append(".")
    with open(f'db/{id}/Model1/{id}.pkl', 'wb') as handle:
        pickle.dump(my_pipe, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return JsonResponse({'status':200})


@api_view(['POST'])
def trainModel(request, id):
    data = json.loads(request.body.decode('utf-8'))
    nn = data['nn']
    comp = data['compile']
    
    params = {'build':[{'dense':{j:p for j,p in v.items() if type(p)!= list}} for k,v in nn.items()]}

    compile_keys = ['loss', 'optimizer', 'metrics']
    fit_keys = ['batch_size', 'epochs', 'validation_split']
    
    params['compile'] = {k:v for k,v in comp.items() if k in compile_keys}
    params['fit'] = {k:v for k,v in comp.items() if k in fit_keys}

    model = Keras_Wrapper(params=params)

    #pull up dataset for this user
    # with open('filename.pkl', 'rb') as handle:
    #     my_pipe = pickle.load(handle)
    
    #pull up pipeline for this user
    import sys
    sys.path.append(".")
    with open(f'db/{id}/Model1/{id}.pkl', 'rb') as handle:
        my_pipe = pickle.load(handle)
    print('MODEL: ',model.skmodel)
    print('pipeline from dir: ', my_pipe)
    return JsonResponse({'status':200})



@api_view(['GET'])
def kerasParameters(request):
    keras_params = list(Keras.objects.all().values())
    df = pd.DataFrame(keras_params)
    entities= list(set(df['entities']))
    data = {}
    for p in entities:
        entity_names = list(set(df.loc[df['entities'] == p]['entity_name'].values)) 
        data[p] = {k:[n for n in df.loc[df['entity_name'] == k]['arg_name'].values] for k in entity_names}
    
    return JsonResponse({"keras": data})
