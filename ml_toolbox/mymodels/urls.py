from django.urls import path
from .views import allPreprocessors
from .views import kerasParameters
from .views import createPipeline
from .views import trainModel
urlpatterns = [
    path('preprocessors/all', allPreprocessors),
    path('keras/all', kerasParameters),
    path('createPreprocessor/<int:id>',createPipeline ),
    path('train/<int:id>', trainModel),
]
