from django.db import models

# Create your models here.
#this django app will house all the db models 


class SklearnPreprocessors(models.Model):
    preprocessor_type = models.CharField(max_length=50)
    preprocessor_name = models.CharField(max_length=50)
    arg_name = models.CharField(max_length=100)
    arg_type =  models.CharField(max_length=100)


class Keras(models.Model):
    entities = models.CharField(max_length = 50)
    entity_name = models.CharField(max_length = 50)
    arg_name = models.CharField(max_length=100)
